//#region Imports
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const Pixel = require('./models/pixel');
const Response = require('./utils/response');
const functions = require('./utils/functions');
const sequelize = require('./utils/database');
const mainRoute = require('./routes/main');
//#endregion

//#region Params
const app = express();
const PORT = 4000;
let online = 0;
let pixelBehaviourInterval = null;
//#endregion

//#region Middlewere
const corsOptions = { origin: '*', optionsSuccessStatus: 200 };



app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//#endregion

// Routes
app.use(mainRoute);
//ERROR HANDELING
app.use((error, req, res, next) =>
{
    res.status(error.statusCode).json(Response(false, null, error.message))
});

//SYNC TO DATABASE
sequelize.sync()
    .then(result => 
    {
        //CREATE SERVER
        const server = app.listen(PORT);
        //CREATE WEBSOCKET
        const io = require('./utils/socket').init(server, corsOptions);
        io.on('connection', socket =>
        {
            online += 1;
            if (online === 1)
                pixelBehaviourInterval = setInterval(() => functions.onPixelsGrowPopulation(), 3000);

            socket.on('disconnect', () =>
            {
                online -= 1;
                if (online === 0)
                {
                    clearInterval(pixelBehaviourInterval);
                    pixelBehaviourInterval = null;
                    functions.resetPixelsPopulation();
                }
            });
        })
    })
    .catch(err => console.log(err));