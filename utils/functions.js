const Pixel = require('../models/pixel');
let lastId = 1000001;

module.exports = {
    onPixelsGrowPopulation: () =>
    {
        const io = require('../utils/socket').getIO();
        const data = [];
        const messages = [];

        Pixel.findAll()
            .then(result =>
            {
                // push all pixel into data array
                result.map(r => { data.push({ id: r.id, r: r.r, g: r.g, b: r.b, parentA_id: r.parent_A_id, parentB_id: r.parent_B_id }) });
                // create index array
                let dataIndex = data.map((d, index) => index);
                // get 2 pixels that will create new baby oixel
                const indexArr = getCouplePixelIndex(dataIndex);
                // get baby pixel details
                const babyPixelData = getBabyPixelDetails([data[indexArr[0]], data[indexArr[1]]]);
                // get baby pixel reports messages
                babyPixelData.messages.map(m => messages.push(m));
                //push baby pixel to total pixels array
                data.push(babyPixelData.data);
                // add baby pixel details to the database
                return Pixel.create(babyPixelData.data)
            })
            .then(result =>
            {
                const responseData = {
                    newPixel: data[data.length - 1],
                    allPixels: data,
                    reports: messages
                }

                io.emit('populationChanged', { action: 'populationGrow', data: responseData });
            })
            .catch(err => { err.statusCode = 422; throw err; });
    },

    resetPixelsPopulation: () =>
    {
        // delete all pixels from database that, not include the base pixels
        Pixel.findAll()
            .then(result =>
            {
                const removePixelsId = []
                result.map(r => { if (r.id !== 10000000 && r.id !== 10000001) removePixelsId.push(r.id); })
                return Pixel.destroy({ where: { id: removePixelsId } })
            })
            .then(result => console.log("database been reseted"))
            .catch(err => console.log(err));
        // reset last pixel id
        lastId = 1000001;
    },
};

const getCouplePixelIndex = (dataIndex) =>
{
    const index_1 = dataIndex[Math.floor((Math.random() * dataIndex.length))];
    dataIndex = dataIndex.filter(d => d !== index_1);
    const index_2 = dataIndex.length === 1 ? dataIndex[0] : dataIndex[Math.floor((Math.random() * dataIndex.length))];

    return [index_1, index_2];
}

const getBabyPixelDetails = (parents) =>
{
    // randomize for each gene if baby pixel ingerite from his parents
    const inheriteGenes = [Math.random() > 0.5, Math.random() > 0.5, Math.random() > 0.5];
    // set info arr for baby pixel gene eaither if he create his own or inherite from his parents
    const genesStatus = getGenesStatus(inheriteGenes)
    // create baby pixel gene
    const babyPixelGene = getbabyPixelGene(genesStatus, parents);
    // create id for baby pixel
    lastId++;
    const randomIdDigit = Math.floor(Math.random() * 10);
    const id = +(lastId.toString() + randomIdDigit.toString())
    // create report of baby pixel gene
    const messages = getRepostsMessages(genesStatus, babyPixelGene);
    // return baby details
    return {
        messages: messages,
        data: {
            id: id,
            r: babyPixelGene[0],
            g: babyPixelGene[1],
            b: babyPixelGene[2],
            parent_A_id: parents[0].id,
            parent_B_id: parents[1].id,
        }
    }
}

const getGenesStatus = (inheriteGenes) =>
{
    return [// -1 = create new gene 1 = inherite from parentA, 2 = inherite from parentB,
        inheriteGenes[0] ? (Math.random() > 0.5 ? 0 : 1) : -1,
        inheriteGenes[1] ? (Math.random() > 0.5 ? 0 : 1) : -1,
        inheriteGenes[2] ? (Math.random() > 0.5 ? 0 : 1) : -1
    ]
}

const getbabyPixelGene = (genesStatus, parents) =>
{
    return [
        genesStatus[0] < 0 ? Math.floor(Math.random() * 256) : parents[genesStatus[0]].r,
        genesStatus[1] < 0 ? Math.floor(Math.random() * 256) : parents[genesStatus[1]].g,
        genesStatus[2] < 0 ? Math.floor(Math.random() * 256) : parents[genesStatus[2]].b,
    ]
}

const getRepostsMessages = (genesStatus, babyPixelGene) =>
{
    const messages = [];
    for (let i = 0; i < genesStatus.length; i++)
    {
        let channel;
        // get gene channel
        switch (i)
        {
            case 0: channel = 'R'; break;
            case 1: channel = 'G'; break;
            case 2: channel = 'B'; break;
        }
        // add current gene report
        switch (genesStatus[i])
        {
            case -1:
                messages.push("pixel create his own " + channel + " gene with value of " + babyPixelGene[i]);
                break;
            case 0:
                messages.push("pixel inherite parentA " + channel + " gene with value of " + babyPixelGene[i]);
                break;
            case 1:
                messages.push("pixel inherite parentB " + channel + " gene with value of " + babyPixelGene[i]);
                break;
        }
    }

    return messages;
}
