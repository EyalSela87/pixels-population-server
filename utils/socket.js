let io;

module.exports = {
    init: (httpServer, corsOptions) =>
    {
        io = require('socket.io')(httpServer, { cors: corsOptions })
        return io;
    },

    getIO: () => 
    {
        if (!io) throw new Error("socket.io is not initialized");
        return io;
    }
}