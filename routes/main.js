const express = require('express');
const mainController = require('../controllers/main');
const router = express.Router();

const Response = require('../utils/response');

router.get('/', mainController.test);

router.get('/getPixels', mainController.getAllPixels)

router.post('/createBasePixels', mainController.createBasePixels);


module.exports = router;