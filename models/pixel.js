const Sequelize = require('sequelize');

const sequelize = require('../utils/database');

const Pixel = sequelize.define('pixel',
    {
        id:
        {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        r: Sequelize.INTEGER,
        g: Sequelize.INTEGER,
        b: Sequelize.INTEGER,
        parent_A_id: Sequelize.INTEGER,
        parent_B_id: Sequelize.INTEGER

    });

module.exports = Pixel;