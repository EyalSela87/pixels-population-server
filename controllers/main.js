const Pixel = require('../models/pixel');
const Response = require('../utils/response');

exports.test = (req, res, next) =>
{
    res.status(200).json(Response(true, { message: "ok" }, null));
}

exports.getAllPixels = (req, res, next) =>
{
    Pixel.findAll()
        .then(result =>
        {
            const data = [];
            result.map(r => { data.push({ id: r.id, r: r.r, g: r.g, b: r.b, parentA_id: r.parent_A_id, parentB_id: r.parent_B_id }) });
            res.status(200).json(Response(true, data, null));
        })
        .catch(err => { err.statusCode = 404; next(err) });
}

exports.createBasePixels = (req, res, next) =>
{
    const data = [
        { id: 10000000, r: 255, g: 255, b: 255, parent_A_id: 0, parent_B_id: 0 },
        { id: 10000001, r: 0, g: 0, b: 0, parent_A_id: 0, parent_B_id: 0 },
    ]

    Pixel.findAll()
        .then(result =>
        {
            if (result.length > 0)
                res.status(200).json(Response(true, "already got base pixels", null));
            else
            {
                Pixel.bulkCreate(data)
                    .then(result =>
                    {
                        res.status(200).json(Response(true, result, null));
                    })
                    .catch(err => { err.statusCode = 422; next(err) });
            }
        })
        .catch(err => { err.statusCode = 422; next(err) });
}